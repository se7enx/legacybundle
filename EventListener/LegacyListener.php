<?php

namespace ThinkCreative\LegacyBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use eZ\Publish\Core\MVC\Legacy\LegacyEvents as eZPublishLegacyEvents;
use eZ\Publish\Core\MVC\Legacy\Event\PreBuildKernelWebHandlerEvent;
use ThinkCreative\LegacyBundle\Services\SettingsInjection;

class LegacyListener implements EventSubscriberInterface
{

    protected $SettingsInjection;

    public function __construct(SettingsInjection $settings_injection) {
        $this->SettingsInjection = $settings_injection;
    }

    public function onPreBuildKernelWebHandler(PreBuildKernelWebHandlerEvent $event) {
        $EventParameters = $event->getParameters();

        $this->SettingsInjection->loadLegacySettings(
            $EventParameters->get('siteaccess')
        );

        $EventParameters->set(
            'injected-settings',
            $this->SettingsInjection->compileInjectedSettings(
                $EventParameters->get('injected-settings')
            )
        );
    }

    public static function getSubscribedEvents() {
        return array(
            eZPublishLegacyEvents::PRE_BUILD_LEGACY_KERNEL_WEB => array(
                'onPreBuildKernelWebHandler', 128
            ),
        );
    }

}
