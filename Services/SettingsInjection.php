<?php

namespace ThinkCreative\LegacyBundle\Services;

use ThinkCreative\LegacyBundle\DependencyInjection\SettingsInjectionInterface;

class SettingsInjection
{

    protected $InjectionHandlers;
    protected $RootDirectory;

    public function __construct($root_directory) {
        $this->InjectionHandlers = array();
        $this->RootDirectory = $root_directory;
    }

    public function addInjectionHandler(SettingsInjectionInterface $injection_handler) {
        $this->InjectionHandlers[] = $injection_handler;
    }

    public function compileInjectedSettings(array $injected_settings = array()) {
        foreach(
            $this->InjectionHandlers as $InjectionHandler
        ) {
            $injected_settings = $this->combineHandlerSettings(
                $injected_settings, $InjectionHandler->getSettings()
            );
        }
        return $injected_settings;
    }

    public function loadLegacySettings(array $siteaccess) {
        if( !function_exists( 'eZDisplayDebug' ) )
        {
            include(
                "$this->RootDirectory/../ezpublish_legacy/kernel/private/classes/global_functions.php"
            );
        }
        \eZExtension::activateExtensions('default');
        \eZSiteAccess::change($siteaccess);
        \eZExtension::activateExtensions('access');
        \eZINI::resetAllInstances(false);
    }

    protected function combineHandlerSettings($injected_settings, array $handler_settings) {
        foreach($handler_settings as $FileName => $SettingBlocks) {
            $LegacySettings = \eZINI::instance($FileName);
            foreach($SettingBlocks as $BlockName => $Settings) {
                foreach($Settings as $VariableName => $VariableValue) {
                    // apply default configuration settings from legacy kernel, where applicable
                    if(
                        $LegacySettings && $LegacySettings->hasVariable($BlockName, $VariableName)
                    ) {
                        if(
                            is_array(
                                $LegacyValue = $LegacySettings->variable($BlockName, $VariableName)
                            )
                        ) {

                            if(
                                current($VariableValue) !== null
                            ) {

                                foreach(
                                    $LegacyValue as $Key => $Value
                                ) {
                                    if(
                                        is_string($Key) || !isset($VariableValue[$Key])
                                    ) {
                                        $VariableValue[$Key] = $Value;
                                    } else {

                                        if(
                                            !in_array(
                                                $Value, $VariableValue
                                            )
                                        ) {
                                            $VariableValue[] = $Value;
                                        }

                                    }
                                }

                            } else {

                                array_shift($VariableValue);

                            }

                        }
                    }
                    // may overwrite preveiously set injected setting value
                    $injected_settings["$FileName/$BlockName/$VariableName"] = $VariableValue;
                }
            }
        }
        return $injected_settings;
    }

}
