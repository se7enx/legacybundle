<?php

namespace ThinkCreative\LegacyBundle\Classes\DependencyInjection;

use ThinkCreative\LegacyBundle\DependencyInjection\SettingsInjectionInterface;

class CustomTagsInjection implements SettingsInjectionInterface
{

    protected $CustomTagsManager;

    public function __construct($customtags_manager) {
        $this->CustomTagsManager = $customtags_manager;
    }

    public function getSettings() {
        $CustomTagDefinitions = $this->CustomTagsManager->getRegisteredItems();

        $CustomTagsConfiguration = array(
            'content.ini' => array(
                'CustomTagSettings' => array(
                    'AvailableCustomTags' => array_keys($CustomTagDefinitions),
                ),
            ),
        );

        foreach(
            $CustomTagDefinitions as $Name => $Definition
        ) {
            $CustomTagsConfiguration['content.ini'][$Name]['CustomAttributes'] = $Definition->getAttributeList();

            foreach($Definition->Attributes as $Identifier => $CustomAttribute) {
                // check if custom attribute has additional configuration
                if($CustomAttribute) {
                    foreach(
                        $CustomAttribute as $Key => $Value
                    ) {
                        if($Value !== NULL) {
                            $CustomTagsConfiguration['ezoe_attributes.ini']["CustomAttribute_$Name" . "_$Identifier"][$this->transformAttributeKey($Key)] = $Value;
                        }
                    }
                }
            }

        }

        return $CustomTagsConfiguration;
    }

    protected function transformAttributeKey($key) {
        return ucfirst(
            // convert underscores to camel-case
            preg_replace_callback(
                '/_([a-z])/', create_function('$c', 'return strtoupper($c[1]);'), $key
            )
        );
    }

}
