<?php

namespace ThinkCreative\LegacyBundle\Classes\DependencyInjection;

use eZ\Publish\Core\MVC\Symfony\SiteAccess;
use ThinkCreative\LegacyBundle\DependencyInjection\SettingsInjectionInterface;

class SiteAccessInjection implements SettingsInjectionInterface
{

    protected $SiteAccess;
    protected $SiteAccessList;
    protected $SiteAccessGroups;
    protected $SiteDesign;

    public function __construct(SiteAccess $siteaccess, $siteaccess_list, $siteaccess_groups, $site_design) {
        $this->SiteAccess = $siteaccess;
        $this->SiteAccessList = $siteaccess_list;
        $this->SiteAccessGroups = $siteaccess_groups;
        $this->SiteDesign = $site_design;
    }

    public function getSettings() {
        $SiteAccessConfiguration = array(
            'site.ini' => array(
                'SiteAccessSettings' => array(
                    'AvailableSiteAccessList' => $this->SiteAccessList,
                    'RelatedSiteAccessList' => $this->SiteAccessList,
                ),
                'SiteSettings' => array(
                    'SiteList' => $this->SiteAccessList,
                ),
            ),
            'content.ini' => array(
                'VersionManagement' => array(
                    'VersionHistoryClass' => array(
                        null, 10,
                    ),
                ),
            ),
        );

        if(
            in_array(
                $this->SiteAccess->name, $this->SiteAccessGroups['frontend_group']
            )
        ) {
            $SiteAccessConfiguration['site.ini']['DesignSettings']['SiteDesign'] = $this->SiteDesign;
            $SiteAccessConfiguration['site.ini']['SiteAccessSettings']['RequireUserLogin'] = false;
        }

        return $SiteAccessConfiguration;
    }

}
