<?php

namespace ThinkCreative\LegacyBundle\DependencyInjection;

interface SettingsInjectionInterface
{

    public function getSettings();

}
